import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(VueRouter);

const routes = [{
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: '/about',
        name: 'About',

        component: () =>
            import ('@/views/About.vue')
    },
    {
        path: '/Clothes-items',
        name: 'ClothesItems',

        component: () =>
            import ('@/components/ClothesItems.vue')
    },
    {
        path: '/ShoesItems',
        name: 'ShoesItems',

        component: () =>
            import ('@/components/ShoesItems.vue')
    },
    {
        path: '/Perfumitems',
        name: 'Perfumitems',

        component: () =>
            import ('@/components/PerfumItems.vue')
    },
    {
        path: '/SkinCareItems',
        name: 'SkinCareItems',

        component: () =>
            import ('@/components/SkinCareItems.vue')
    },
]
const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;